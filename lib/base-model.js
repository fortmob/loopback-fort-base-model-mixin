'use strict';

// import _debug from './debug';
// const debug = _debug();
var _ = require('underscore');

module.exports = function (Model, options) {

    var app = null;
    var typeName = Model.modelName;
    var options = Model.settings;

    Model._setupRemoting = Model.setupRemoting;  // Backup for parent method
    Model._crudGET = _crudGET;
    Model.crudGET = crudGET;
    Model.crudGETDetail = crudGETDetail;
    Model.deleteDetail = deleteDetail;
    Model.editDetail = editDetail;
    Model.postDetail = postDetail;
    Model.pagerDetail = pagerDetail;
    Model.pager = pager;
    Model._pager = _pager;
    Model.autocomplete = autocomplete;
    Model.autocompleteDetail = autocompleteDetail;
    Model.autocompleteDetailEdit = autocompleteDetailEdit;
    Model.autocompleteGeneral = autocompleteGeneral;
    Model.quickAdd = quickAdd;

    /////////////

    initBaseModel();

    function initBaseModel() {
        if (Model.custom == undefined) {
            Model.custom = {};
            Model.custom.autocomplete = { 'a': 'b' };
        }

        Model.setupRemoting();

        setRemoting(Model, 'pager', {
            description: 'Retrieves rows with pager settings',
            accepts: [{
                arg: 'req',
                type: 'object',
                model: typeName,
                http: {
                    source: 'req'
                }
            }],
            returns: {
                arg: 'rows',
                type: typeName,
                root: true,
                description:
                ('Return rows retrieved'),
            },
            http: {
                verb: 'get',
                path: '/pager'
            }
        });
        setRemoting(Model, 'pagerDetail', {
            description: 'Retrieves rows with pager settings',
            accepts: [{
                arg: 'req',
                type: 'object',
                model: typeName,
                http: {
                    source: 'req'
                }
            }],
            returns: {
                arg: 'rows',
                type: typeName,
                root: true,
                description:
                ('Return rows retrieved'),
            },
            http: {
                verb: 'get',
                path: '/details/:id/:detail/pager'
            }
        });
        setRemoting(Model, 'postDetail', {
            isStatic: false,
            http: { verb: 'post', path: '/details/:detail/:id' },
            accepts: ([{
                arg: 'data',
                type: 'object',
                model: typeName,
                http: { source: 'req' },
            },]),
            //   description: format('Creates a new instance in %s of this model.', relationName),
            accessType: 'WRITE',
            returns: { arg: 'data', type: typeName, root: true }
        });
        
        setRemoting(Model, 'deleteDetail', {
            isStatic: false,
            http: { verb: 'delete', path: '/details/:id/:detail/:iddetail' },
            accepts: ([{
                arg: 'data',
                type: 'object',
                model: typeName,
                http: { source: 'req' },
            },]),
            //   description: format('Creates a new instance in %s of this model.', relationName),
            accessType: 'WRITE',
            returns: { arg: 'data', type: typeName, root: true }
        });
        setRemoting(Model, 'editDetail', {
            isStatic: false,
            http: { verb: 'put', path: '/details/:id/:detail/:iddetail' },
            accepts: ([{
              arg: 'data',
              type: 'object',
              model: typeName,
              http: { source: 'req' },
            }, ]),
            //   description: format('Creates a new instance in %s of this model.', relationName),
            accessType: 'WRITE',
            returns: { arg: 'data', type: typeName, root: true }
        });
        setRemoting(Model, 'autocomplete', {
            description: 'retrieves options for autocomplete field',
            accepts: ([
                {
                    arg: 'params',
                    type: 'object',
                    required: true,
                    http: function (ctx) {
                        var req = ctx.req.params;
                        return req;
                    }
                },
                {
                    arg: 'req',
                    type: 'object',
                    required: false,
                    http: { source: 'req' }
                }
                // { arg: 'accessToken', type: 'object', http: function (ctx) { return ctx.req.accessToken; } }
            ]),
            returns: {
                arg: 'rows',
                type: 'object',
                root: true,
                description:
                ('Return rows retrieved'),
            },
            http: {
                verb: 'get',
                path: '/autocomplete/:field/:search'
            }
        });
        setRemoting(Model, 'autocompleteDetail', {
            description: 'retrieves options for autocomplete field',
            accepts: ([
                {
                    arg: 'params',
                    type: 'object',
                    required: true,
                    http: function (ctx) {
                        var req = ctx.req.params;
                        return req;
                    }
                },
                {
                    arg: 'req',
                    type: 'object',
                    required: false,
                    http: { source: 'req' }
                }
                // { arg: 'accessToken', type: 'object', http: function (ctx) { return ctx.req.accessToken; } }
            ]),
            returns: {
                arg: 'rows',
                type: 'object',
                root: true,
                description:
                ('Return rows retrieved'),
            },
            http: {
                verb: 'get',
                path: '/details/:detail/autocomplete/:field/:search'
                // Route::get($route["route"] . "/details/{detail}/autocomplete/{field}/{search?}", $route["controller"] . '@autocompleteDetail');
            }
        });

        setRemoting(Model, 'autocompleteDetailEdit', {
            description: 'retrieves options for autocomplete field',
            accepts: ([
                {
                    arg: 'params',
                    type: 'object',
                    required: true,
                    http: function (ctx) {
                        var req = ctx.req.params;
                        return req;
                    }
                },
                {
                    arg: 'req',
                    type: 'object',
                    required: false,
                    http: { source: 'req' }
                }
            ]),
            returns: {
                arg: 'rows',
                type: 'object',
                root: true,
                description:
                ('Return rows retrieved'),
            },
            http: {
                verb: 'get',
                path: '/details/:id/:detail/autocomplete/:field/:search'
            }
        });
        
        setRemoting(Model, 'autocompleteGeneral', {
            description: 'retrieves options for autocomplete model without relations',
            accepts: ([
                {
                    arg: 'params',
                    type: 'object',
                    required: true,
                    http: function (ctx) {
                        var req = ctx.req.params;
                        return req;
                    }
                },
                {
                    arg: 'req',
                    type: 'object',
                    required: false,
                    http: { source: 'req' }
                }
                // { arg: 'accessToken', type: 'object', http: function (ctx) { return ctx.req.accessToken; } }
            ]),
            returns: {
                arg: 'rows',
                type: 'object',
                root: true,
                description:
                ('Return rows retrieved'),
            },
            http: {
                verb: 'get',
                path: '/general/autocomplete/:model/:search'
            }
        });
        setRemoting(Model, 'crudGET', {
            description: 'Find a model instance by {{id}} from the data source.',
            accessType: 'READ',
            accepts: ([{
                arg: 'id',
                type: 'any',
                description: 'Model id',
                required: true,
                http: { source: 'path' }
            },]),
            returns: { arg: 'data', type: typeName, root: true },
            http: { verb: 'get', path: '/crudGET/:id' }
        });

        setRemoting(Model, 'crudGETDetail', {
            description: 'Find a model instance by {{id}} from the data source.',
            accessType: 'READ',
            accepts: ({
                arg: 'req',
                type: 'object',
                model: typeName,
                http: {
                    source: 'req'
                }
            }),
            returns: { arg: 'data', type: typeName, root: true },
            http: { verb: 'get', path: '/details/:id/:detail/:iddetail' },
        });


        // ======== quickAdd ======== //
        // Use example
        // Model/quickAdd/field  -  content in POST body
        // medicamentos/quickAdd/tipomedicamento -  {"nome":"Genérico"} (body)
        setRemoting(Model, 'quickAdd', {
            description: 'Quick Add.',
            accessType: 'READ',
            accepts: ([{
                arg: 'data',
                type: 'object',
                model: typeName,
                http: {
                    source: 'req',
                },
            }]),
            accessType: 'WRITE',
            returns: {
                arg: 'data',
                type: typeName,
                root: true,
            },
            http: {
                verb: 'post',
                path: '/quickAdd/:field',
            },
        });
    }
    function setRemoting(scope, name, options) {
        var fn = scope[name];
        // fn._delegate = true;
        options.isStatic = scope === Model;
        Model.remoteMethod(name, options);
    }

    function crudGETDetail(req, fn) {
        var params = req.params;
        var id = params.id;
        var iddetail = params.iddetail;

        var relation = getRelation(Model, params.detail);

        var model = Model.app.models[relation.model];

        return this._crudGET(model, iddetail, fn)
    }
    function crudGET(id, fn) {
        return this._crudGET(Model, id, fn)
    }
    function _crudGET(Model, id, fn) {
        var modelName = Model.definition.name;
        var relations = Model.definition.settings.relations;

        var includes = [];
        var labels = [];
        // if (relations.length > 0) {
        _.each(relations, function (rel, key) {
            if (rel.type == 'belongsTo' || rel.type == 'hasMany') {
                if (rel.type == 'belongsTo') {
                    labels.push(key);
                }
                var nestedRelations = Model.app.models[rel.model].settings.relations;
                var _customIncludes = Model.app.models[rel.model].customIncludes;
                var nestedIncludes = [];
                _.each(nestedRelations, function (nRel, nKey) {
                    if (nRel.type == 'belongsTo') {
                        nestedIncludes.push(nKey);
                    }

                    if (nRel.type == 'hasMany') {
                        _.each(_customIncludes, function (model) {
                            if (nRel.model === model) {
                                // Check inner relations
                                var innerNestedRelations = Model.app.models[nRel.model].settings.relations;
                                var innerNestedIncludes = [];

                                _.each(innerNestedRelations, function (inRel, inKey) {
                                    if (inRel.type === 'belongsTo') {
                                        innerNestedIncludes.push(inKey);
                                    }
                                });

                                if (innerNestedIncludes.length > 0) {
                                    var _nestedObj = {};
                                    _nestedObj[nKey] = innerNestedIncludes;
                                    nestedIncludes.push(_nestedObj);
                                } else nestedIncludes.push(nKey);
                            }
                        });
                    }
                });
                if (nestedIncludes.length == 0) {
                    includes.push(key);
                } else {
                    var _o = {};
                    _o[key] = nestedIncludes;
                    includes.push(_o);
                }
            }
        });

        Model.findById(id, {
            include: includes
        }, function (err, row) {
            if (err) {
                fn(err)
            }

            var newRow = (JSON.parse(JSON.stringify(row)));

            // console.log(newRow);

            includes.forEach(function (include) {
                if (typeof include == 'string') {
                    var __relation = relations[include];
                    if (__relation != undefined) {
                        if (__relation.type == 'belongsTo') {
                            if (newRow[include] != undefined) {
                                if (Model.custom.autocomplete[include] !== undefined) {
                                    newRow[include + '.label'] = Model.custom.autocomplete[include].label(newRow);
                                }
                            }
                        } else if (__relation.type == 'hasMany') {
                            console.log('esse caso ainda não foi implementado.', __relation);
                        }
                    }
                } else if (typeof include == 'object') {
                    // include.forEach(function(val, key) {
                    _.each(include, function (val, key) {
                        var __relation = relations[key];
                        if (__relation != undefined) {
                            if (__relation.type == 'belongsTo') {
                                if (newRow[key] != undefined) {
                                    newRow[key + '.label'] = Model.custom.autocomplete[key].label(newRow);
                                }
                            } else if (__relation.type == 'hasMany') {
                                var subModel = Model.app.models[__relation.model];
                                if (typeof val == 'array' || typeof val == 'object') {
                                    // esse caso pode ser recursivo ou pode ser um objeto
                                    // newRow[key].foreach(function(_newRow) {
                                    if (newRow != null) {
                                        _.each(newRow[key], function (_newRow, _newRowKey) {
                                            _.each(val, function (_include) {
                                                if (_newRow[_include] != undefined) {
                                                    // console.log('return', subModel.custom.autocomplete[_include].label(_newRow));
                                                    // console.log(subModel.modelName, _include);
                                                    if (subModel.custom.autocomplete[_include] !== undefined) {
                                                        _newRow[_include + '.label'] = subModel.custom.autocomplete[_include].label(_newRow);
                                                    }
                                                }
                                            })

                                            newRow[key][_newRowKey] = _newRow;
                                        });
                                    }
                                } else {
                                    console.log('esse caso ainda não foi implementado 2: ', val)
                                }
                            }
                        }
                    })
                }

            });

            fn(null, newRow);
        });
    }
    function getRelations(model) {
        return model.definition.settings.relations;
    }
    function getRelation(model, relationName) {
        var relations = model.definition.settings.relations;
        if (relations[relationName]) {
            var _relation = relations[relationName];
            return _relation;
        }
    }
    function deleteDetail(req, fn) {
        var detail = req.params.detail;
        var id = req.params.id;
        var iddetail = req.params.iddetail;

        var relation = getRelation(Model, detail);
        var model = Model.app.models[relation.model];

        var where = {
            id: iddetail,
        }
        where[relation.foreignKey] = id;

        model.destroyAll(where, function (err, success) {
            if (err) {
                fn(err);
            } else {
                fn(null, 'success');
            }
        });
    }
    function editDetail(req, fn) {
        var detail = req.params.detail;
        var id = req.params.id;
        var iddetail = req.params.iddetail;
        var data = req.body;

        var relation = getRelation(Model, detail);
        var model = Model.app.models[relation.model];
        var nestedRelations = model.settings.relations;

        // Get objects relations id and assign to foreignKey attribute
        if (nestedRelations && Object.keys(nestedRelations).length > 0) {
            Object.keys(nestedRelations).forEach(function (_relation) {
                if (data[_relation] && typeof data[_relation] === 'object') {
                    data[nestedRelations[_relation].foreignKey] = data[_relation].id;
                }
                if (data[_relation] && typeof data[_relation] === 'number') {
                    data[nestedRelations[_relation].foreignKey] = data[_relation];
                }
            });
        }

        var where = {
          id: iddetail,
        }
        where[relation.foreignKey] = id;

        model.upsertWithWhere(where, data, function(err, success) {
        if (err) {
          fn(err);
        } else {
          fn(null, 'success');
        }
      });
    }
    function postDetail(req, fn) {
        var detail = req.params.detail;
        var id = req.params.id;

        var data = req.body;

        var relation = getRelation(Model, detail);
        var model = Model.app.models[relation.model];

        data[relation.foreignKey] = id;
        model.create(data, function (err, success) {
            if (err) {
                fn(err);
            } else {
                fn(null, 'success');
            }
        });
    }
    function pager(req, fn) {
        this._pager(Model, req, fn);
    }
    function pagerDetail(req, fn) {
        var params = req.params;
        var id = params.id;

        var relation = getRelation(Model, params.detail);

        var model = Model.app.models[relation.model];

        model._crudWhere = {};

        model._crudWhere[relation.foreignKey] = id;

        this._pager(model, req, fn);
    }
    function _pager(model, req, fn) {
        var query = req.query;
        query.page = query.page || 1;

        if (model._crudWhere == undefined) {
            model._crudWhere = {};
        }

        var where = Object.assign({}, model._crudWhere);
        where.customWHERE = [];

        if (query.filter != undefined){
            _.each(query.filter, function (val, key) {
                
                if (model.definition.settings.relations[key]){
                    where[model.definition.settings.relations[key].foreignKey] = parseInt(val);
                
                }else if (model.definition.properties[key]){
                    var prop = model.definition.properties[key];

                    if (prop.type.name == 'Number') {
                        where[key] = parseInt(val);
                    
                    }else if (prop.type.name == 'String'){
                        where[key] = { ilike: '%' + val + '%'};
                    
                    }else if (prop.type.name == 'Boolean'){
                        where[key] = val=="true" ? true : false;
                    
                    }else if (prop.type.name == 'Date'){

                        if (typeof(val)=="object"){

                            if (val.ini && val.fim){
                                where[key] = {between: [ formatDate(val.ini), formatDate(val.fim)]};
                                return;
                            }

                            var and={};

                            if (val.ini){
                                and.gte = formatDate(val.ini);
                            }

                            if (val.fim){
                                and.lte = formatDate(val.fim);
                            }

                            where[key] = and;

                        }else{
                            where[key] = formatDate(val); 
                        }

                    }else{
                        where[key] = val;
                    }

                }
            });

        }

        if (query.q != undefined && query.q.trim().length > 0) {
            //console.log(query.q);
            var table = model.definition.name.toLowerCase();
            var q = query.q.trim().replace(/'/g, "");
            var rels = getRelations(model);
            var props = _.clone(model.definition.properties);

            var arrORS = [];

            _.each(props, function (prop, key) {
                if (prop['type'] === String){
                    var tmp = {};
                    tmp[key.toLowerCase()] = { ilike: '%' + q + '%'};
                    where.customWHERE.push(tmp);
                }
            });
        }

        if (where.customWHERE.length > 0){
            where = {
                or: where.customWHERE
            };
        }

        var _scope = null;

        if (query.scope != undefined) {
            _scope = query.scope;
        }

        var limit = 20;

        var count = model.count(where,function (err, count) {
            if (err) {
                // console.log(err);
                fn(err);
            } else {
                var modelName = model.definition.name;

                var relations = getRelations(model);

                var includes = [];
                // if (relations.length > 0) {
                _.each(relations, function (rel, key) {
                    if (rel.type == 'belongsTo' || (model.customIncludes != undefined && model.customIncludes.indexOf(key) != -1)) {
                        var nestedRelations = Model.app.models[rel.model].settings.relations;

                        var nestedIncludes = [];
                        _.each(nestedRelations, function (nRel, nKey) {
                            if (nRel.type == 'belongsTo') {
                                nestedIncludes.push(nKey);
                            }
                        });

                        if (nestedIncludes.length == 0) {
                            includes.push(key);
                        } else {
                            var _o = {};
                            _o[key] = nestedIncludes;
                            includes.push(_o);
                        }
                    }
                });

                var _sort = 'createdAt DESC';

                if (query.sort_by != undefined && query.order != undefined) {
                    var arrSort = query.sort_by.split('.label');
                    if (arrSort.length>1){
                        _sort = relations[arrSort[0]].foreignKey + ' ' + query.order;
                    }else{
                        _sort = query.sort_by + ' ' + query.order;
                    }
                }

                var filter = {
                    include: includes,
                    limit: limit,
                    skip: (query.page - 1) * limit,
                    order: _sort,
                    where: where
                };

                if (_scope == null)
                    _scope = 'find';

                var rows = model[_scope](filter, function (err, rows) {
                    if (err) {
                        fn(err);
                    } else {
                        var newRows = [];

                        rows.forEach(function (row) {
                            var newRow = (JSON.parse(JSON.stringify(row)));
                            includes.forEach(function (relation) {
                                var relationName = typeof relation == 'string' ? relation : Object.keys(relation)[0];
                                if (model.custom.autocomplete[relationName] != undefined && newRow[relationName] != undefined) {
                                    try {
                                        newRow[relationName + '.label'] = model.custom.autocomplete[relationName].label(newRow);
                                    } catch (err) {
                                        newRow[relationName + '.label'] = newRow[relationName];
                                        console.log('ERROR on AUTOCOMPLETE:label -> ' + modelName + ':' + relationName);
                                    }
                                }
                            });

                            newRows.push(newRow);
                        });

                        var out = {
                            data: newRows,
                            page: req.page,
                            per_page: 20,
                            total_entries: rows.length,
                            total_count: count,
                            total_pages: Math.ceil(count / limit)
                        }

                        fn(null, out);
                    }
                });
            }
        });
    }
    function _autocomplete(modelName, params, req, fn) {
        console.log('AUTOCOMPLETE CALL', modelName, params);

        var _model = Model.app.models[modelName];
        var relations = Model.app.models[modelName].definition.settings.relations;
        var _field = params.field;

        if (!_model.custom.autocomplete[_field]) {
            fn('autocomplete settings not defined. check custom.autocomplete');
        }

        if (_model.custom.autocomplete[_field].customQuery!==undefined){
            _model.custom.autocomplete[_field].customQuery(modelName, params, req, fn);
            return;
        }

        var autocompleteSettings = _model.custom.autocomplete[_field];

        if (relations[_field]) {
            var _relation = relations[_field];
            var relationModel = Model.app.models[_relation.model];
        } else {
            if (!autocompleteSettings.model) {
                fn('autocomplete model not defined. check custom.autocomplete');
            }
            var relationModel = Model.app.models[autocompleteSettings.model];
        }

        var _where = {};
        var _search = params.search == '[blank]' ? '' : params.search;

        var reg = new RegExp(_search, 'i');

        if (autocompleteSettings.include == undefined) {
            autocompleteSettings.include = [];
        }

        _where = {
            where: autocompleteSettings.where(reg, req),
            include: autocompleteSettings.include
        };

        if (params.search == '[blank]') {
            _.each(_where.where, function (val, key) {
                if (val === reg) {
                    delete _where.where[key];
                }
            })
        }

        if (req.query.limit!=0){
            if (req.query.limit==undefined){
                _where.limit = 10;
            }else{
                _where.limit = req.query.limit;
            }
        }

        // Filtering cases
        if(req.query && typeof req.query === 'object' && Object.keys(req.query).length > 0) {
            _where = {
                include: autocompleteSettings.include,
                where: {
                    and: []
                }
            };

            var _autoSettings = autocompleteSettings.where(reg, req);
            var orderDefault;
            
            Object.keys(_autoSettings).forEach(function (attr) {
                var _obj = {};
                if (attr === 'where' && typeof _autoSettings[attr] === 'object') {
                    _where.where.and.push(_autoSettings[attr]);
                } else {
                    _obj[attr] = _autoSettings[attr];
                    _where.where.and.push(_obj);
                }
            });

            // SET ORDER IN AUTOCOMPLETE
            if(_autoSettings[Object.keys(_autoSettings)[0]] instanceof RegExp){
                orderDefault = Object.keys(_autoSettings)[0] + ' ASC'
            }
            
            _where.order = autocompleteSettings.order ? autocompleteSettings.order : orderDefault;
            _where.limit = autocompleteSettings.limit ? autocompleteSettings.limit : 20;
            
            var promises = [];

            Object.keys(req.query).forEach(function (attr) {                
                var _obj = {};
                _obj[attr] = parseInt(req.query[attr]);
                _where.where.and.push(_obj);                
            });
            
            if (promises.length > 0) {
                Promise.all(promises)
                    .then(function success(response) {
                        var _prestId = [];
                        response.forEach(function (rows) {
                            rows.forEach(function (_row) {
                                _prestId.push(_row.pres_id);
                            });
                        });

                        _where.where.and.push(
                            { id: { inq: _prestId } },
                            { ativo: true }
                        );

                        relationModel.find(_where, { 'unaccent': true, 'queryType': 'SELECT' }, function (err, result) {
                            if (err) {
                                fn(err);
                            }

                            autocompleteOut(result, fn);
                        });
                    });
            } else {
                
                relationModel.find(_where, { 'unaccent': true, 'queryType': 'SELECT' }, function (err, result) {
                    if (err) {
                        fn(err);
                    }

                    autocompleteOut(result, fn);
                });
            }
        }

        else {
            relationModel.find(_where, { 'unaccent': true, 'queryType': 'SELECT' }, function (err, result) {
                if (err) {
                    fn(err);
                }

                autocompleteOut(result, fn);
            });
        }

        function autocompleteOut(result, fn) {
            var out = [];
            result.forEach(function (row) {
                row = autocompleteSettings.out(row, req);
                if (row){
                    out.push(row);
                }
            });

            fn(null, out);
        }
    }
    function autocompleteDetail(params, req, fn) {
        var relations = Model.definition.settings.relations;
        var detail = params.detail;
        if (relations[detail]) {
            var modelName = relations[detail].model;

            _autocomplete(modelName, params, req, fn);
        }
    }

    function autocompleteDetailEdit(params, req, fn){
        var relations = Model.definition.settings.relations;
        var detail = params.detail;
        if (relations[detail]) {
            var modelName = relations[detail].model;
            _autocomplete(modelName, params, req, fn);
        }
    }

    function autocomplete(params, req, fn) {
        var modelName = Model.definition.name;
        _autocomplete(modelName, params, req, fn);
    }
    function autocompleteGeneral(params, req, fn) {
        console.log('GENERAL AUTOCOMPLETE!', params.model, params);

        var modelName = params.model;
        var _model = Model.app.models[modelName];

        var reg = new RegExp(params.search, 'i');

        var autocompleteSettings = _model.custom.autocompleteGeneral;

        if (!autocompleteSettings) fn('autocomplete settings not defined. check custom.autocomplete');

        var _where = {
            where: autocompleteSettings.where(reg, req)
        };

        if (params.search == '[blank]') {
            _.each(_where.where, function (val, key) {
                if (val === reg) {
                    delete _where.where[key];
                }
            });
        }

        _where.limit = 10;
  
        if(req.query && typeof req.query === 'object' && Object.keys(req.query).length > 0) {
            var __where = {
                where: {
                    and: []
                }
            };

            Object.keys(_where).forEach(function (attr) {
                var _obj = {};
                if (attr === 'where' && typeof _where[attr] === 'object') {
                    __where.where.and.push(_where[attr]);
                } else {
                    _obj[attr] = _where[attr];
                    __where.where.and.push(_obj);
                }
            });

            var promises = [];

            Object.keys(req.query).forEach(function (attr) {
                // Enter inside PrestadorEspecialidades to get prestador IDs
                if (attr === 'especialidade' && modelName === 'AtendimentoEncaminhamento') {
                    promises.push(Model.app.models['PrestadorEspecialidade'].find(
                        { where: { esp_id: req.query[attr] } }
                    ));
                }

                else {
                    var _obj = {};
                    _obj[attr] = parseInt(req.query[attr]);
                    __where.where.and.push(_obj);

                    // Add inner relation from DocumentoMedico
                    if (modelName === 'DocumentoMedico') {
                        __where.include = ['documento_tipo'];
                    }
                }
            });

            if (promises.length > 0) {
                Promise.all(promises)
                    .then(function success(response) {
                        var _prestId = [];
                        response.forEach(function (rows) {
                            rows.forEach(function (_row) {
                                _prestId.push(_row.pres_id);
                            });
                        });

                        __where.where.and.push({
                            id: {
                                inq: _prestId
                            }
                        });

                        _model.find(__where, { 'unaccent': true, 'queryType': 'SELECT' }, function (err, result) {
                            if (err) {
                                fn(err);
                            }
                            // console.log(result);

                            var out = [];
                            result.forEach(function (row) {
                                out.push(autocompleteSettings.out(row));
                            });

                            fn(null, out);
                        });
                    });
            } else {
                _model.find(__where, { 'unaccent': true, 'queryType': 'SELECT' }, function (err, result) {
                    if (err) {
                        fn(err);
                    }
                    // console.log(result);

                    var out = [];

                    result.forEach(function (row) {
                        out.push(autocompleteSettings.out(row));
                    });

                    fn(null, out);
                });
            }
        }

        else {
            _model.find(_where, { 'unaccent': true, 'queryType': 'SELECT' }, function (err, result) {
                if (err) {
                    fn(err);
                }
                // console.log(result);

                var out = [];

                result.forEach(function (row) {
                    out.push(autocompleteSettings.out(row));
                });

                fn(null, out);
            });
        }
    }
    function quickAdd(req, fn) {
        // field to add 
        var field = req.params.field;
        // data (model content)
        var data = req.body;
        // get relation
        var relation = getRelation(Model, field);
        var model = app.models[relation.model];
        // create
        model.create(data, function (err, success) {
            if (err) {
                fn(err);
            } else {
                fn(null, success);
            }
        });
    }

    Model.observe('before save', function beforeSave(ctx, next) {
        var app = ctx.Model.app;
        var modelName = ctx.Model.definition.name;

        if (ctx.instance) { // for single model update
            // Get the Application object which the model attached to, and we do what ever we want
            var s = ctx.instance;
            ctx.options.originalData = s.__data;

            var relations = ctx.Model.definition.settings.relations;

            if (_.toArray(relations).length > 0) {
                for (var y in relations) {
                    var relation = relations[y];
                    if (relation.type == 'hasMany') {
                        // next();
                    } else {
                        var fkField = s.__data[relation.foreignKey];

                        if (relation.options == undefined) {
                            relation.options = {
                                required: false
                            };
                        }

                        if (relation.options.required == undefined) {
                            relation.options.required = false;
                        }

                        if (fkField !== undefined && fkField !== null && fkField !== '') {
                            if (relation.options.required && (fkField == undefined || fkField == null)) {

                                var createErr = new Error(relation.foreignKey + " can't be blank");
                                createErr.statusCode = 422;
                                createErr.code = 'CANT_SAVE_MODEL';

                                next(createErr);
                            } else {
                                // app.models[relation.model].exists(fkField, function(err, exists) {
                                //     if (err) throw err;
                                //     if (!exists) {
                                //         var createErr = new Error("Reference for Foreign Key " + relation.foreignKey + " for model " + relation.model + " does not exist");
                                //         createErr.statusCode = 422;
                                //         createErr.code = 'CANT_SAVE_MODEL';

                                //         next(createErr);
                                //     }
                                // });
                            }
                        }

                        if (ctx.instance[y + '.label'] != undefined) {                            
                            if(ctx.instance[y + '.label'] == ''){
                                ctx.instance[relation.foreignKey] = null;
                            }else{
                                ctx.instance[relation.foreignKey] = ctx.instance[y + '.label'].id;
                            }        
                        }

                    }
                };
                // found 0 validation errors. proceed
                next();
            } else {
                next();
            }
        } else if (ctx.currentInstance) {
            var relations = ctx.Model.definition.settings.relations;
            var _data = ctx.data;

            ctx.options.originalData = _data;

            if (_.toArray(relations).length > 0) {
                for (var y in relations) {
                    var relation = relations[y];
                    if (relation.type == 'hasMany') {
                        // for (var z in _data[y]) {
                        //     var detail = _data[y][_z];
                        //     ctx.currentInstance[y].create()
                        // }
                        // next();
                    } else {
                        var fkField = _data[relation.foreignKey];

                        if (relation.options == undefined) {
                            relation.options = {
                                required: false
                            };
                        }

                        if (relation.options.required == undefined) {
                            relation.options.required = false;
                        }

                        if (fkField !== undefined && fkField !== null && fkField !== '') {
                            if (relation.options.required && (fkField == undefined || fkField == null)) {

                                var createErr = new Error(relation.foreignKey + " can't be blank");
                                createErr.statusCode = 422;
                                createErr.code = 'CANT_SAVE_MODEL';

                                next(createErr);
                            } else {
                                app.models[relation.model].exists(fkField, function (err, exists) {
                                    if (err) throw err;
                                    if (!exists) {
                                        var createErr = new Error("Reference for Foreign Key " + relation.foreignKey + " for model " + relation.model + " does not exist");
                                        createErr.statusCode = 422;
                                        createErr.code = 'CANT_SAVE_MODEL';
                                        next(createErr);
                                    }
                                });
                            }
                        }
                        if (_data[y + '.label'] != undefined) {
                            if(_data[y + '.label'] == ''){
                                ctx.data[relation.foreignKey] = null;
                            }else{
                                ctx.data[relation.foreignKey] = _data[y + '.label'].id;
                            }                            
                        }
                    }
                };
                // found 0 validation errors. proceed
                next();
            } else {
                next();
            }
        } else {
            next();
        }
    });
    Model.observe('after save', function afterSave(ctx, next) {
        var originalData = (ctx.options.originalData);
        var app = ctx.Model.app;
        var modelName = ctx.Model.definition.name;

        if (ctx.instance) { // for single model update
            var relations = ctx.Model.definition.settings.relations;
            var _data = ctx.instance;

            originalData = _.extend(originalData, ctx.instance.__data, ctx.instance.__cachedRelations);

            // busca registros antigos (edição)
            app.models[modelName].findById(_data.id, function (err, row) {
                if (err) {
                    console.log(err);
                }

                _.each(relations, function (relation, y) {
                    if (relation.type == 'hasMany') {
                        var _model = app.models[relation.model];
                        var keepIds = [];
                        var promises = [];

                        if (row == null) {
                            insertNewRelationData();
                        } else {
                            if (row[y] != undefined && row[y] != null) {
                                row[y]({}, function (err, old) {
                                    if (originalData[y] != undefined) {
                                        _.each(originalData[y], function (detail, z) {
                                            if (detail.id != null || detail.id != undefined) {
                                                keepIds.push(detail.id);
                                            }
                                        });
                                    }

                                    console.log('model: ' + modelName, 'relation-model: ' + relation.model, 'keepIds', keepIds);

                                    var filterId = {};
                                    filterId[relation.foreignKey] = row.id;

                                    insertNewRelationData();

                                    Promise.all(promises)
                                        .then(function (savedModels) {
                                            if (originalData[y] != undefined) {
                                                _model.destroyAll({
                                                    and: [{
                                                        id: { nin: keepIds }
                                                    },
                                                        filterId
                                                    ]
                                                },
                                                    function (err, success) {
                                                        if (err) throw err;
                                                    });
                                            }

                                        })
                                        .catch(function (err) {
                                            console.log(err);
                                        })
                                });
                            }
                        }

                        function insertNewRelationData() {
                            // for (var z in originalData[y]) {
                            if (originalData[y] != undefined) {
                                _.each(originalData[y], function (detail, z) {

                                    var _promise = new Promise(function (resolve, reject) {
                                        // console.log(detail);
                                        detail[relation.foreignKey] = row.id;

                                        // app.models[relation.model]
                                        _model.upsert(detail, function (err, ret) {
                                            if (err) reject(err);
                                            keepIds.push(ret.id);
                                            console.log('after insert new relation data:', ret)
                                            resolve(ret);
                                        })
                                    });
                                    promises.push(_promise);
                                })
                            }
                        }
                    }
                });
            });
            next();
        } else {
            next();
        }
    });

    function formatDate(data, formato) {
        if (formato == 'pt-br') {
          return (data.substr(0, 10).split('-').reverse().join('/'));
        } else {
          var quebra = data.substr(0, 10).split('/');
          return [quebra[1],quebra[0], quebra[2]].join('-');
        }
    }
    return Model;
};